INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/localizer.h \
    $$PWD/dispatcher.h \
    $$PWD/localizerpool.h \
    $$PWD/classifier.h \
    $$PWD/classifierpool.h \
    $$PWD/importer.h \
    $$PWD/cvhelper.h \
    $$PWD/naturalsort.h \
    $$PWD/mainwindow.h \
    $$PWD/galleryview.h

SOURCES += \
    $$PWD/localizer.cpp \
    $$PWD/dispatcher.cpp \
    $$PWD/localizerpool.cpp \
    $$PWD/classifier.cpp \
    $$PWD/classifierpool.cpp \
    $$PWD/importer.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/galleryview.cpp
FORMS += \
    $$PWD/mainwindow.ui \
    $$PWD/galleryview.ui
