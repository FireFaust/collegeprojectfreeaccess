#include "classifier.h"

Classifier::Classifier(QObject *parent) :
    QObject(parent)
{
    transformG = br::Transform::fromAlgorithm("GenderEstimation");
    transformA = br::Transform::fromAlgorithm("AgeEstimation");
}

void Classifier::processFrameFace(DbImage frame, int threadNr)
{
    if(!frame.getImg().empty())
    for(int i = 0; i < frame.faces.size(); i++){

//        if(!frame.faces[i].getImg(frame.getImg()).empty())
          cv::Mat img = frame.faces[i].getImg(frame.getImg());
        if(!img.empty()){
            br::Template queryA(img);
            queryA >> *transformG;
            br::Template queryB(img);
            queryB >> *transformA;
            QString genderS = queryA.file.get<QString>("Gender");
            double age = queryB.file.get<QString>("Age").toDouble();
            int gender = genderS == "Male"?0:1;

            frame.faces[i].age = age;
            frame.faces[i].gender = gender;

        }
    }
    emit frameFaceProcessed(frame, threadNr);
}
