#ifndef CLASSIFIERPOOL_H
#define CLASSIFIERPOOL_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <deque>
#include <vector>
#include <classifier.h>

class ClassifierPool : public QObject
{
    Q_OBJECT
public:
    explicit ClassifierPool(QObject *parent = 0);

signals:
    void finished();
    void frameProcessed(DbImage frame);
    void frameClassified(int queue);

public slots:
    void doTerminate();
    void processFrame(DbImage frame);

private slots:
    void frameFaceProcessed(DbImage frame, int threadNr);

private:

    void localProcessFrame();

    std::deque<DbImage> frameQueue;
//    DbImage currFrame;
    int busyThreads;

    std::vector<QThread *> classifierThreads;
    std::vector<Classifier *> classifiers;
    std::vector<bool> classifierBusy;
    std::deque<qint64> timing;
    qint64 lastFrameProcessed;

    qint64 lastFrameTimestamp;

    int threadCount;
    int maxTimingFrames;

    bool mustTerminate;
};

#endif // CLASSIFIERPOOL_H
