#include "dispatcher.h"

#include <QDebug>

Dispatcher::Dispatcher(int &argc, char **argv) :
    QApplication(argc, argv)
{
    frameLocalizedDebug = false;

    qRegisterMetaType<SmartFrame>("SmartFrame");
    qRegisterMetaType<DbFace>("DbFace");
    qRegisterMetaType<DbImage>("DbImage");

    localizerPool.moveToThread(&localizerPoolThread);
    classifierPool.moveToThread(&classifierPoolThread);
    importer.moveToThread(&importerThread);

    connect(&localizerPool, &LocalizerPool::finished, &localizerPoolThread, &QThread::quit);
    connect(&classifierPool, &ClassifierPool::finished, &classifierPoolThread, &QThread::quit);
    connect(&importer, &Importer::finished, &importerThread, &QThread::quit);


    connect(&localizerPool, &LocalizerPool::frameProcessed, &classifierPool, &ClassifierPool::processFrame);
    connect(&classifierPool, &ClassifierPool::frameClassified, &localizerPool, &LocalizerPool::frameClassified);

    connect(&localizerPool,SIGNAL(progress(int,int)),this,SLOT(progressBar(int,int)));

    localizerPoolThread.start();
    classifierPoolThread.start();
    importerThread.start();

}

Dispatcher::~Dispatcher()
{
    localizerPool.doTerminate();
    classifierPool.doTerminate();

}

void Dispatcher::processGallery(qint64 galleryId)
{
    QMetaObject::invokeMethod(&localizerPool, "processGallery", Qt::QueuedConnection, Q_ARG( qint64, galleryId));
}

void Dispatcher::progressBar(int curr, int total)
{
    emit progress(curr, total);
//    if(curr == total)
//    {
//        localizerPool.doTerminate();
//        classifierPool.doTerminate();
//    }
}

void Dispatcher::setClassifier(int type)
{
    localizerPool.setClassifier(type);
}
