#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QObject>
#include <QApplication>
#include <QThread>

#include "smartframe.h"
#include "localizerpool.h"
#include "classifierpool.h"
#include "importer.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "Struct/mydb.h"
#include "Struct/dbface.h"

class Dispatcher : public QApplication
{
    Q_OBJECT
public:
    explicit Dispatcher(int &argc, char **argv);
    ~Dispatcher();
    Importer importer;
    void setClassifier(int type);

signals:
    void progress(int curr, int total);

public slots:
    void processGallery(qint64 galleryId);
    void progressBar(int curr, int total);

private slots:

private:

    LocalizerPool localizerPool;
    QThread localizerPoolThread;

    ClassifierPool classifierPool;
    QThread classifierPoolThread;

    QThread importerThread;
    MyDb db;

    bool frameLocalizedDebug;
};

#endif // DISPATCHER_H
