#include "idassigner.h"
#include "opencv2/highgui/highgui.hpp"

IdAssigner::IdAssigner(QObject *parent) :
    QObject(parent)
{
    frameTimeout = 2000;
    currMaxId = 1;

    dataDir = QDir(QDir::homePath().append("/temp/SSFaces/"));
    if(!dataDir.mkpath(".")){
        qCritical() << "Unable to create data dir";
    }

    jpegParams.push_back(CV_IMWRITE_JPEG_QUALITY);
    jpegParams.push_back(88);
}

void IdAssigner::doTerminate()
{
    emit(finished());
}

void IdAssigner::assignIds(SmartFrame frame)
{

    // Clear previous and old data
    idAssigned. clear();;
    for(int i = 0; i < frame.faceCenters.size(); i++){
        idAssigned.push_back(false);
        frame.faceId.push_back(-1);
        frame.age.push_back(-1);
        frame.gender.push_back(-1);
        frame.ageConf.push_back(0.);
        frame.genderConf.push_back(0.);
        frame.agePrecise.push_back(-1);
    }

    std::list<IdPos>::iterator it = history.begin();
    while(it != history.end())
    {
        (*it).candidateNr = -1;
        (*it).candidateDist = INT_MAX;
        (*it).candidateLocked = false;

        if(frame.timestamp - (*it).lastSeen > frameTimeout){
            it = history.erase(it);
        }else{
            it++;
        }
    }

    bool hasCandidates = true;
    while(hasCandidates){
        hasCandidates = false;

        for(int i = 0; i < frame.faceCenters.size(); i++){
            if(idAssigned[i]) continue;
            for(std::list<IdPos>::iterator h = history.begin(); h != history.end(); ++h){
                if((*h).candidateLocked) continue;
                int dist = cv::norm((*h).pos-frame.faceCenters[i]) ;
                if(dist < frame.faceSize[i]*2.5 //Should config dist here (currently based on face size)
                        && dist <  (*h).candidateDist){
                    (*h).candidateDist = dist;
                    (*h).candidateNr = i;
                    hasCandidates = true;
                }
            }
        }

        for(int i = 0; i < frame.faceCenters.size(); i++){
            if(idAssigned[i]) continue;

            qint64 bestTime = 0;
            int bestDist = INT_MAX;
            std::list<IdPos>::iterator bestMach;
            bool matchFound  = false;

            for(std::list<IdPos>::iterator h = history.begin(); h != history.end(); ++h){
                if((*h).candidateLocked) continue;
                if((*h).candidateNr != i) continue;
                if((*h).lastSeen < bestTime) continue;
                if((*h).lastSeen == bestTime && (*h).candidateDist > bestDist) continue;

                matchFound = true;
                bestTime = (*h).lastSeen;
                bestMach = h;
                bestDist = (*h).candidateDist;
            }

            if(matchFound){
                idAssigned[i] = true;
                (*bestMach).candidateLocked = true;
                (*bestMach).lastSeen = frame.timestamp;
                (*bestMach).pos = frame.faceCenters[i];
                frame.faceId[i] = (*bestMach).id;
            }
        }
    }

    for(int i = 0; i < idAssigned.size(); i++){
        if(idAssigned[i]) continue;

        IdPos newPos;
        newPos.id = currMaxId++;
        newPos.lastSeen = frame.timestamp;
        newPos.pos = frame.faceCenters[i];
        history.push_back(newPos);

        frame.faceId[i] = newPos.id;

        cv::imwrite(dataDir.absoluteFilePath(QString("%1").arg(newPos.id).append(".jpg")).toStdString(),frame.img(frame.faces[i]),jpegParams);

    }

    emit frameProcessed(frame);
}
