#ifndef IMPORTER_H
#define IMPORTER_H

#include <QObject>
#include <QDebug>
#include <QDirIterator>
#include <qalgorithms.h>
#include <naturalsort.h>

class Importer : public QObject
{
    Q_OBJECT
public:
    explicit Importer(QObject *parent = 0);

signals:
    void finished();
    void progress(int curr, int total);

public slots:
    void doTerminate();
    void importDir(QString path, QString gallery, bool recursive = true);
    void importVideo(QString path, QString galleryName);

private:
    bool importing;
};

#endif // IMPORTER_H
