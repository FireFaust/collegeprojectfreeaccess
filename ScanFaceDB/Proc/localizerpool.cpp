#include "localizerpool.h"

LocalizerPool::LocalizerPool(QObject *parent) :
    QObject(parent),
    currImageInList(0)
{
    threadCount = QThread::idealThreadCount();
    maxQueue = threadCount;
    maxTimingFrames =  2*threadCount;
    lastFrameProcessed = 0;
    lastFrameTimestamp = 0;
    currImagesProcessing = 0;
    mustTerminate = false;
    QThread * tThread;
    Localizer * tLocalizer;
    for(int i = 0; i < threadCount; i++){
        tThread = new QThread();
        tLocalizer = new Localizer();
        localizerThreads.push_back(tThread);
        tLocalizer->moveToThread(tThread);
        localizers.push_back(tLocalizer);
        localizerBusy.push_back(false);
        tThread->start();

        connect(tLocalizer,&Localizer::frameProcessed,this,&LocalizerPool::frameProcessedLocaly);
    }
}

void LocalizerPool::frameClassified(int queue)
{  
    emit progress(currImagesProcessing+currImageInList -queue,imageList.size());
}

void LocalizerPool::doTerminate()
{
    mustTerminate = true;
    for(int i = 0; i < threadCount; i++){
        if(localizerBusy[i]){
            return;
        }
    }
    emit finished();
}

void LocalizerPool::processGallery(qint64 galleryId)
{
    if(mustTerminate) return;

    imageList = DbImage::getList(galleryId);
    if(imageList.size() == 0) {
        qDebug() << "No images in gallery";
        return;
    }
    currImageInList = 0;
    currImagesProcessing = 0;
    processFrames();
}


void LocalizerPool::processFrames()
{
    if(currImageInList >= imageList.size()) return;
    for(int i = 0; i < threadCount; i++){
        if(!localizerBusy[i] && currImageInList < imageList.size()){
            localizerBusy[i] = true;
            currImagesProcessing ++;
            DbImage frame = imageList[currImageInList++];
            QMetaObject::invokeMethod(localizers[i], "processFrame", Qt::QueuedConnection, Q_ARG(DbImage, frame), Q_ARG(int, i));
        }
    }
}

void LocalizerPool::frameProcessedLocaly(DbImage frame, int threadNr)
{
    localizerBusy[threadNr] = false;
    currImagesProcessing --;

    if(mustTerminate){
        bool canTerminate = true;
        for(int i = 0; i < threadCount; i++){
            if(localizerBusy[i]) canTerminate = false;
        }
        if(canTerminate) emit(finished());
        return;
    }

    qint64 currTime =  QDateTime::currentMSecsSinceEpoch(), diff;

    if(lastFrameProcessed != 0){
        diff = currTime - lastFrameProcessed;
        if(diff < 5000){
            timing.push_back(diff);
            if(timing.size()>maxTimingFrames) timing.pop_front();

            qint64 tTime = 0;
            for(std::deque<qint64>::iterator it = timing.begin(); it != timing.end(); ++it){
                tTime += *it;
            }
            tTime /= timing.size();
        }
    }

    lastFrameProcessed = currTime;

    emit frameProcessed(frame);
    qDebug() << "LocalizerPool::frameProcessed";
    processFrames();
}

void LocalizerPool::setClassifier(int type)
{
    for(int i = 0; i < threadCount; i++)
    {
        localizers.at(i)->setClassifier(type);
    }
}
