#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    busy(false)
{
    ui->setupUi(this);
    dispacher = (Dispatcher*)QApplication::instance();

    progressBar = new QProgressBar(ui->statusBar);
    progressBar->setAlignment(Qt::AlignRight);

    progressLabel = new QLabel(ui->statusBar);
    progressLabel->setAlignment(Qt::AlignRight);

    ui->statusBar->addWidget(progressBar,1);
    progressBar->setVisible(busy);
    progressBar->setValue(0);
    progressBar->setMaximum(1000);
    progressBar->setMinimum(0);

    ui->statusBar->addWidget(progressLabel,0);
    progressLabel->setText("");
    progressLabel->setVisible(busy);

    ui->galleryList->verticalHeader()->hide();

    ui->galleryList->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
//    ui->galleryList->horizontalHeader()->setStretchLastSection(true);

    QHeaderView* header = ui->galleryList->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::Stretch);

    connect(ui->menuBar, &QMenuBar::triggered,this, &MainWindow::menuAction);
    connect(&dispacher->importer, &Importer::progress,this, &MainWindow::importProgress);
    connect(dispacher,SIGNAL(progress(int,int)),this,SLOT(workProgress(int,int)));
    //connect(ui->galleryList, &QTableWidget::cellDoubleClicked,this, &MainWindow::cellDoubleClicked);
    connect(ui->btnProcessGallery,&QPushButton::clicked, this, &MainWindow::btnProcessGalleryClicked);
    connect(ui->btnClearFaces,&QPushButton::clicked, this, &MainWindow::btnClearFacesClicked);
    connect(ui->btnReview,&QPushButton::clicked, this, &MainWindow::btnReviewClicked);
    connect(ui->btnDelete,&QPushButton::clicked,this,&MainWindow::btnDeleteGalleryClicked);
    reloadGalleries();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::menuAction(QAction *action)
{
    if(action == ui->actionImport_video){
        importVideo();
    }
    if(action == ui->actionImport_folder){
        importFolder();
    }
    if(action == ui->actionReturn){
        ui->pages->setCurrentWidget(ui->mainPage);
    }
    if(action == ui->actionImport_Classifier){
        importClassifier();
    }
    if(action == ui->actionDefault_Classifier)
    {
        defaultClassifier();
    }
}

void MainWindow::setBusy(bool busy)
{
    this->busy = busy;

    progressLabel->setVisible(busy);
    progressBar->setVisible(busy);
    if(busy){
        progressBar->setValue(0);
        progressBar->setMaximum(1000);
        progressBar->setMinimum(0);
    }else{
        reloadGalleries();
    }
}

void MainWindow::importProgress(int curr, int total)
{    
    if(curr == 0 && total == 0){
        setBusy(false);
        reloadGalleries();
        return;
    }

    if(curr == -1){
        progressBar->setMaximum(0);
        progressLabel->setText(QString("Processed:")+QString::number(total));
    }
}

void MainWindow::workProgress(int cur, int total)
{
    if(cur <80)
    {
        setBusy(true);
        progressBar->setMaximum(total);
    }
    progressLabel->setText(QString("processed:")+QString::number(cur));
    progressBar->setValue(cur);
    if(cur>=total)
        setBusy(false);
}

void MainWindow::importFolder()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setNameFilter(tr("Folders"));
    dialog.setDirectory(QDir::homePath());

    QGridLayout* dialogLayout = dynamic_cast<QGridLayout*>(dialog.layout());
    QComboBox * gallery;
    QCheckBox * recursive;
    QLabel * galleryLabel;
    if (  dialogLayout ) {
        gallery = new QComboBox(&dialog);
        gallery->setEditable(true);
        galleryLabel = new QLabel("Gallery:",&dialog);
        gallery->addItems(DbGallery::getNameList());
        recursive = new QCheckBox("Recursive:",&dialog);
        recursive->setChecked(true);
        dialogLayout->addWidget(gallery,5,1);
        dialogLayout->addWidget(galleryLabel,5,0);
        dialogLayout->addWidget(recursive,6,1);
    }else{
        return;
    }

    if (!dialog.exec())
    {
        return;
    }

    int dialogResult = QMessageBox::No;
    QMessageBox msgBox;
    msgBox.setText("Gallery name not provided.");
    msgBox.setInformativeText("Use foldername as gallery name?");
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::Yes);

    while (gallery->currentText().isEmpty() && dialogResult == QMessageBox::No) {
        if(gallery->currentText().isEmpty()) dialogResult = msgBox.exec();
    }

    QStringList fileNames = dialog.selectedFiles();
    if(fileNames.size() != 1){
        QMessageBox msgBox(this);
        msgBox.setText("Invalid directory! State not saved!");
        msgBox.exec();
        return;
    }

    if(gallery->currentText().isEmpty()){
        gallery->setCurrentText(QFileInfo(fileNames[0]).completeBaseName());
    }

    QMetaObject::invokeMethod(&dispacher->importer, "importDir", Qt::QueuedConnection, Q_ARG( QString, fileNames[0]), Q_ARG( QString, gallery->currentText()), Q_ARG( bool, recursive->isChecked()));
    setBusy(true);
}

void MainWindow::importVideo()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Videos (.webm *.mkv *.flv *.flv *.ogv *.ogg *.drc *.mng *.avi *.mov *.qt *.wmv *.rm *.rmvb *.asf *.mp4 *.m4p *.m4v *.mpg *.mp2 *.mpeg *.mpg *.mpe *.mpv *.mpg *.mpeg *.m2v *.m4v *.svi *.3gp *.3g2 *.mxf *.roq *.nsv), All ()"));
    dialog.setDirectory(QDir::homePath());

    QGridLayout* dialogLayout = dynamic_cast<QGridLayout*>(dialog.layout());
    QComboBox * gallery;
    QLabel * galleryLabel;
    if (  dialogLayout ) {
        gallery = new QComboBox(&dialog);
        gallery->setEditable(true);
        galleryLabel = new QLabel("Gallery:",&dialog);
        gallery->addItem("");
        gallery->addItems(DbGallery::getNameList());
        dialogLayout->addWidget(gallery,5,1);
        dialogLayout->addWidget(galleryLabel,5,0);
    }else{
        return;
    }

    //gallery->setCurrentText("");
    int dialogResult = QMessageBox::No;
    QMessageBox msgBox;
    msgBox.setText("Gallery name not provided.");
    msgBox.setInformativeText("Use filename as gallery name?");
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::Yes);


    while (gallery->currentText().isEmpty() && dialogResult == QMessageBox::No) {
        if (!dialog.exec())
        {
            return;
        }
        if(gallery->currentText().isEmpty()) dialogResult = msgBox.exec();
    }

    QStringList fileNames = dialog.selectedFiles();
    if(fileNames.size() != 1){
        QMessageBox msgBox(this);
        msgBox.setText("Invalid directory! State not saved!");
        msgBox.exec();
        return;
    }

    if(gallery->currentText().isEmpty()){
        gallery->setCurrentText(QFileInfo(fileNames[0]).completeBaseName());
    }


    QMetaObject::invokeMethod(&dispacher->importer, "importVideo", Qt::QueuedConnection, Q_ARG( QString, fileNames[0]), Q_ARG( QString, gallery->currentText()));
    setBusy(true);
}

void MainWindow::reloadGalleries()
{
    ui->galleryList->clear();
    galleryList = DbGallery::getList();

    int row = 0,col=0;
    ui->galleryList->setRowCount(galleryList.size());
    ui->galleryList->setColumnCount(4);
    ui->galleryList->setHorizontalHeaderItem(col++,new QTableWidgetItem("ID"));
    ui->galleryList->setHorizontalHeaderItem(col++,new QTableWidgetItem("Name"));
    ui->galleryList->setHorizontalHeaderItem(col++,new QTableWidgetItem("Image Count"));
    ui->galleryList->setHorizontalHeaderItem(col++,new QTableWidgetItem("Objects"));

    for(std::vector<DbGallery>::iterator it = galleryList.begin(); it != galleryList.end(); ++it){
        col = 0;
        ui->galleryList->setItem(row, col++, new QTableWidgetItem(QString::number((*it).getDbID())));
        ui->galleryList->setItem(row, col++, new QTableWidgetItem((*it).name));
        ui->galleryList->setItem(row, col++, new QTableWidgetItem(QString::number((*it).getImageCount())));
        ui->galleryList->setItem(row, col++, new QTableWidgetItem(QString::number((*it).getFacesCount())));
        row++;
    }

    //ui->galleryList->resizeColumnsToContents();
}

void MainWindow::btnProcessGalleryClicked()
{
    QList<QTableWidgetItem*> selection  = ui->galleryList->selectedItems();
    if(selection.empty()) return;
    int row = ui->galleryList->row(selection.first());
    qint64 id = galleryList[row].getDbID();
    processGallery(id);

}

void MainWindow::processGallery(qint64 galleryId)
{
    dispacher->processGallery(galleryId);
}

void MainWindow::btnClearFacesClicked()
{
    QList<QTableWidgetItem*> selection  = ui->galleryList->selectedItems();
    if(selection.empty()) return;
    int row = ui->galleryList->row(selection.first());
    qint64 id = galleryList[row].getDbID();

    DbFace::clearInGallery(id);
    reloadGalleries();
}

void MainWindow::btnReviewClicked(){
    QList<QTableWidgetItem*> selection  = ui->galleryList->selectedItems();
    if(selection.empty()) return;
    int row = ui->galleryList->row(selection.first());
    qint64 id = galleryList[row].getDbID();

    ui->galleryView->loadGallery(id);
    ui->galleryPage->show();
    ui->pages->setCurrentWidget(ui->galleryPage);
}

void MainWindow::btnDeleteGalleryClicked()
{
    QList<QTableWidgetItem*> selection  = ui->galleryList->selectedItems();
    if(selection.empty()) return;
    int row = ui->galleryList->row(selection.first());
    qDebug() << "Delete Gallery" <<galleryList[row].deleteAll();
    reloadGalleries();
}

void MainWindow::returnToOriginal()
{
    ui->pages->setCurrentWidget(ui->mainPage);
}

void MainWindow::importClassifier()
{
    QString classifier = QFileDialog::getOpenFileName(this, tr("Import Cascade"),
                                                    QDir::homePath(),tr("cascades(*.xml)"));
    if(classifier!="")
    {
        QFile file(classifier);
        if(QFileInfo(QDir::current().absoluteFilePath("new_cascade.xml")).exists())
            QFile::remove("new_cascade.xml");
        file.copy(QDir::current().absoluteFilePath("new_cascade.xml"));
    }
    dispacher->setClassifier(NO_FACE);
}

void MainWindow::defaultClassifier()
{
    dispacher->setClassifier(FACE);
}
