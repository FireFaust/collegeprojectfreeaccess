#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>
#include <QLabel>
#include "dispatcher.h"
#include "dbgallery.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void menuAction(QAction *action);
    void importProgress(int curr, int total);
    void btnProcessGalleryClicked();
    void btnClearFacesClicked();
    void btnReviewClicked();
    void btnDeleteGalleryClicked();
    void returnToOriginal();
    void workProgress(int, int);
    void importClassifier();
    void defaultClassifier();
private:
    Ui::MainWindow *ui;
    Dispatcher * dispacher;
    QProgressBar * progressBar;
    QLabel * progressLabel;
    std::vector<DbGallery> galleryList;

    void importFolder();
    void importVideo();
    void reloadGalleries();

    void setBusy(bool busy);
    bool busy;

    void processGallery(qint64 galleryId);
};

#endif // MAINWINDOW_H
