#-------------------------------------------------
#
# Project created by QtCreator 2014-10-29T11:23:43
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ScanFaceDB
TEMPLATE = app


SOURCES += main.cpp\

INCLUDEPATH += /usr/include
LIBS += -L/usr/lib
LIBS += -ljpeg
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

LIBS += -lopenbr

include(settings/settings.pri)
include(Proc/Proc.pri)
include(Struct/Struct.pri)

RESOURCES += \
    src.qrc

