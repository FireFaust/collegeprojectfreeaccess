INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/smartframe.h \
    $$PWD/mydb.h \
    $$PWD/dbimage.h \
    $$PWD/dbgallery.h \
    $$PWD/dbface.h \
    $$PWD/parsedfaces.h \
    $$PWD/structs.h \
    $$PWD/imagegraphicsview.h \
    $$PWD/imagegraphicsscene.h

SOURCES += \
    $$PWD/smartframe.cpp \
    $$PWD/mydb.cpp \
    $$PWD/dbimage.cpp \
    $$PWD/dbgallery.cpp \
    $$PWD/dbface.cpp \
    $$PWD/parsedfaces.cpp \
    $$PWD/imagegraphicsview.cpp \
    $$PWD/imagegraphicsscene.cpp

