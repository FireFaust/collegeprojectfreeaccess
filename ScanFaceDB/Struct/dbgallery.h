#ifndef DBGALLERY_H
#define DBGALLERY_H

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <QStringList>

#include "dbimage.h"

class DbGallery
{
public:

    DbGallery();

    QString name;

    bool save();

    bool load(qint64 dbID);
    bool load(QString name);
    void loadImages();
    void loadImagesAndFaces();
    bool deleteAll();

    inline qint64 getDbID(){return dbID;}
    inline qint64 getImageCount(){return imageCount;}
    inline qint64 getFacesCount(){return facesCount;}
    static DbGallery get(qint64 dbID);

    static std::vector<DbGallery> getList();
    static QStringList getNameList();

    std::vector<DbImage> images;

private:

    qint64 dbID;
    qint64 imageCount;
    qint64 facesCount;
};

#endif // DBGALLERY_H
