#include "dbimage.h"


QDir DbImage::imageDir(QDir::homePath().append(QString("/SanFaceDB_Images/")));

DbImage::DbImage()
{
    dbID = -1;
    galleryId = 0;
}


QString DbImage::imageAbsolutePath(qint64 id)
{
    QDir initial(imageDir.absolutePath().append("/images/"));
    if(!initial.mkpath(".")){
        return QString();
    }
    return imageDir.absolutePath().append("/images/%1.jpg").arg(id);
}

bool DbImage::save()
{
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO images (galleryId) "
                      "VALUES (:galleryId)");
    }else{
        query.prepare("UPDATE images SET "
                      "galleryId = :galleryId "
                      "WHERE id = :id"
                      );
        query.bindValue(":id", dbID);
    }

    query.bindValue(":galleryId", galleryId);

    if(!query.exec()){
        qDebug() << query.lastError().text() << ": "<< query.executedQuery();
        return false;
    }
    if(dbID == -1) dbID = query.lastInsertId().toInt();

    for(std::vector<DbFace>::iterator it = faces.begin(); it != faces.end(); ++it){
        it->imageId = dbID;
        it->save();
    }

    if(!img.empty())
    {

        QFileInfo file(imageAbsolutePath(dbID));

        if(file.exists()){
            return true;
        }

        std::vector<int> params;
        params.push_back(cv::IMWRITE_JPEG_QUALITY);
        params.push_back(96);

        cv::imwrite( file.absoluteFilePath().toStdString().c_str(), img,params);
        img = cv::Mat();
    }

    return true;
}

bool DbImage::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images WHERE id = :id");
    query.bindValue(":id", dbID);
    faces.clear();

    if(query.exec() && query.first()){
        this->dbID = query.value("id").toInt();
        this->galleryId = query.value("galleryId").toInt();

        QFileInfo file(imageAbsolutePath(dbID));
        if(!file.exists()){
            return false;
        }

        img = cv::imread(file.absoluteFilePath().toStdString().c_str());

        // Also loads faces
        if(!loadFaces()) return false;

        return true;
    }
    return false;
}

bool DbImage::loadFaces()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM faces WHERE imageId = :imageId ");
    query.bindValue(":imageId", getDbID());

    if(!query.exec()){
        qDebug() << "DbImage::load error - " << query.lastError().text() << query.executedQuery() ;
        return false;
    }

    DbFace obj;
    while (query.next()) {
        if(!obj.load(query)){
            continue;
        }
        faces.push_back(obj);
    }
    return true;
}

bool DbImage::deleteAll()
{

    QFile::remove(imageAbsolutePath(dbID));

    QSqlQuery query;
    query.prepare("DELETE FROM faces WHERE imageId = :id");
    query.bindValue(":id", dbID);
    if(!query.exec())
        return false;

    query.prepare("DELETE FROM images WHERE galleryId = :id");
    query.bindValue(":id", galleryId);
    if(!query.exec())
        return false;



    return true;
}

DbImage DbImage::get(qint64 dbID)
{
    DbImage image;
    image.load(dbID);
    return image;
}

cv::Mat DbImage::getImg()
{
    if(img.empty() && getDbID() > 0 && galleryId > 0)
    {

        QFileInfo file(imageAbsolutePath(getDbID()));
        if(!file.exists()){           
            return cv::Mat();
        }

        cv::Mat tImg = cv::imread(file.absoluteFilePath().toLocal8Bit().constData());
        return tImg;
    }
    return img;
}



std::vector<DbImage> DbImage::getList(qint64 galleryId)
{
    QSqlQuery query;
    std::vector<DbImage> result;
    query.prepare("SELECT * FROM images WHERE galleryId = :galleryId ");
    query.bindValue(":galleryId", galleryId);

    if(!query.exec()){
        qDebug() << "DbImage::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    DbImage tDbImage;

    while (query.next()) {
        tDbImage.dbID = query.value("id").toInt();
        tDbImage.galleryId = query.value("galleryId").toInt();
        result.push_back(tDbImage);
    }
    return result;
}

void DbImage::setImg(cv::Mat img)
{
    this->img = img;
}
