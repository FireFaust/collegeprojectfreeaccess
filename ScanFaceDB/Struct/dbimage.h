#ifndef DBIMAGE_H
#define DBIMAGE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <QDir>

#include <QFile>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "dbface.h"

class DbImage
{
public:
    static QDir imageDir;

    DbImage();

    qint64 galleryId;

    cv::Mat getImg();
    void setImg(cv::Mat img);
    std::vector<DbFace> faces;

    bool save();

    bool load(qint64 dbID);
    bool loadFaces();
    bool deleteAll();

    inline qint64 getDbID(){return dbID;}
    static DbImage get(qint64 dbID);

    static std::vector<DbImage> getList(qint64 galleryId);

    static QString imageAbsolutePath(qint64 id);

private:
    qint64 dbID;
    cv::Mat img;
};

#endif // DBIMAGE_H
