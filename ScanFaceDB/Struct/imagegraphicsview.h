#ifndef IMAGEGRAPHICSVIEW_H
#define IMAGEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>
#include <QDebug>

class ImageGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit ImageGraphicsView(QWidget *parent = 0);

signals:
    void faceSelected(bool status);

public slots:
    void zoomTo(QRect rect,QString zoomIn_or_Out);

protected:
    virtual void wheelEvent(QWheelEvent* event);

    void mousePressEvent(QMouseEvent * event);
};

#endif // IMAGEGRAPHICSVIEW_H
