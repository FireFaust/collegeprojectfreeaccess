#ifndef MYDB_H
#define MYDB_H

#include <QObject>

#include <QSql>
#include <QDir>
#include <QSqlDatabase>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

class MyDb : public QObject
{
    Q_OBJECT
public:
    explicit MyDb(QObject *parent = 0);

    inline bool isOk(){return ok;}

signals:

public slots:

private:
    QSqlDatabase db;
    QSettings settings;
    bool ok;

    bool checkTables();
};

#endif // MYDB_H
