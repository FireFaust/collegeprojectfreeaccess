#include "parsedfaces.h"

ParsedFaces::ParsedFaces()
{
    minFaceOverlap = 0.2;
}

void ParsedFaces::setMinFaceOverlap(float minFaceOverlap)
{
    this->minFaceOverlap = minFaceOverlap;
}


bool ParsedFaces::AddFaceFromMat(cv::Mat *src, cv::Rect local_position, cv::Rect global_position, int angle)
{
    /// Check if faces overlaps existing regions
    for(unsigned int i = 0; i < faces.size(); i++){
        if(
                faces[i].position.x > global_position.x+global_position.width ||
                faces[i].position.x + faces[i].position.width  < global_position.x ||
                faces[i].position.y > global_position.y+global_position.height ||
                faces[i].position.y + faces[i].position.height  < global_position.y )
            continue;
        /// Calculate ammount of overlap
        int overlap = (std::min(faces[i].position.x + faces[i].position.width, global_position.x + global_position.width)-
                       std::max(faces[i].position.x, global_position.x)) *
                (std::min(faces[i].position.y + faces[i].position.height, global_position.y + global_position.height)-
                 std::max(faces[i].position.y, global_position.y));
        /// Check if less than treshold
        if (overlap == 0 || (float)overlap/(global_position.width*global_position.height) < minFaceOverlap)
            continue;
        /// Return if overlapps
        // TODO pick with 90' degree rotation angle for better quality
        if(faces[i].angle % 90 != 0 && angle % 90 == 0)
        {
            faces.erase(faces.begin()+i);
            break;
        }
        return false;
    }

    double scalePercent = 1;
    cv::Rect scaledPosition;
    scaledPosition.width = local_position.width * scalePercent;
    scaledPosition.height = local_position.height * scalePercent;
    scaledPosition.x = local_position.x + (local_position.width - scaledPosition.width)/2;
    scaledPosition.y = local_position.y + (local_position.height - scaledPosition.height)/2;

    ParsedFace face;
    face.img = cv::Mat(scaledPosition.size(), CV_8UC1);
    (*src)(scaledPosition).copyTo(face.img);
    face.position = global_position;
    face.angle = angle;

    cv::normalize( face.img, face.img,0,255,cv::NORM_MINMAX, CV_8UC1);

    faces.push_back(face);
    return true;
}

