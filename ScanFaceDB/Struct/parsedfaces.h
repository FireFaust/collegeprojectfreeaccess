#ifndef PARSEDFACES_H
#define PARSEDFACES_H

#include <vector>

#include "opencv2/imgproc/imgproc.hpp"

// For development only
#include <cstdio>

struct ParsedFace{
    cv::Rect position;
    cv::Mat img;
    int angle;
};

class ParsedFaces
{
public:
    ParsedFaces();
    bool AddFaceFromMat(cv::Mat * src, cv::Rect local_position, cv::Rect global_position, int angle);
    void clear(){faces.clear();}
    ParsedFace operator [](int i) const {return faces[i];}
    std::vector<ParsedFace>::size_type size() {return faces.size();}
    //TODO shuld be private
    std::vector<ParsedFace> faces;
    void setMinFaceOverlap(float minFaceOverlap);
private:
    float minFaceOverlap;
};

#endif // PARSEDFACES_H
