#include "optcascade.h"

OptCascade::OptCascade()
{
    setDefaults();
}

OptCascade::OptCascade(QJsonValue value)
{
    setJSON(value);
}

OptCascade::OptCascade(QString path, double scaleStep, int minNeighbors, int flags, cv::Size minSize, bool hasMaxSize, cv::Size maxSize)
{
    this->path = path;
    this->scaleStep = scaleStep;
    this->minNeighbors = minNeighbors;
    this->flags = flags;
    this->minSize = minSize;
    this->hasMaxSize = hasMaxSize;
    this->maxSize = maxSize;
}

void OptCascade::setDefaults()
{
    path = "/usr/local/share/OpenCV/lbpcascades/lbpcascade_frontalface.xml";
    scaleStep = 1.25;
    minNeighbors = 4;
    flags = cv::CASCADE_SCALE_IMAGE | cv::CASCADE_FIND_BIGGEST_OBJECT;
    minSize = cv::Size(50,50);
    hasMaxSize = false;
    maxSize = cv::Size(9999,9999);
}

bool OptCascade::setJSON(QJsonValue value)
{
    QJsonArray tArr;
    QJsonValue tVal;
    QJsonObject tObj;
    QString tStr;
    int tInt;

    if(!value.isObject()){
        qWarning("Opt: Invalid type for rotations child - must be object (hint: check for trailing comas)");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== scale_step
    if(!getDouble(opt,scaleStep,"scale_step")) return false;
    //=============================================================== path
    if(!getString(opt,path,"path",true)) return false;
    //=============================================================== min_neighbors
    if(!getInt(opt,minNeighbors,"min_neighbors")) return false;
    //=============================================================== flags
    if(!getArray(opt,tArr,"flags")) return false;
    tInt = 0;
    for(int i = 0; i < tArr.size(); i ++)
    {
        tVal = tArr[i];
        if(!tVal.isString()){
            qWarning("Opt: Invalid type for flag - must be string (hint: check for trailing comas)");
            return false;
        }
        tStr = tVal.toString();

        if(tStr == "CASCADE_SCALE_IMAGE"){
            tInt |= cv::CASCADE_SCALE_IMAGE;
        } else if(tStr == "CASCADE_DO_CANNY_PRUNING"){
            tInt |= cv::CASCADE_DO_CANNY_PRUNING;
        } else if(tStr == "CASCADE_DO_ROUGH_SEARCH"){
            tInt |= cv::CASCADE_DO_ROUGH_SEARCH;
        } else if(tStr == "CASCADE_FIND_BIGGEST_OBJECT"){
            tInt |= cv::CASCADE_FIND_BIGGEST_OBJECT;
        }
    }
    flags = tInt;
    //=============================================================== min_size
    int width,height;
    bool set;
    if(!getObject(opt,tObj,"min_size",false,set)) return false;
    if(set){
        if(!getInt(tObj,width,"width",true)) return false;
        if(!getInt(tObj,height,"height",true)) return false;
        minSize = cv::Size(width,height);
    }
    //=============================================================== has_max_size
    if(!getBool(opt,hasMaxSize,"has_max_size")) return false;
    //=============================================================== max_size
    if(!getObject(opt,tObj,"max_size",false,set)) return false;
    if(set){
        if(!getInt(tObj,width,"width",true)) return false;
        if(!getInt(tObj,height,"height",true)) return false;
        maxSize = cv::Size(width,height);
    }
    return true;
}

QJsonValue OptCascade::getJSON()
{
    QJsonObject tObj, tObj2;
    QJsonArray tArr;
    tObj.insert("path",path);
    tObj.insert("scale_step",scaleStep);
    tObj.insert("min_neighbors",minNeighbors);

    tArr = QJsonArray();
    if(flags & cv::CASCADE_SCALE_IMAGE) tArr.push_back(QString("CASCADE_SCALE_IMAGE"));
    if(flags & cv::CASCADE_DO_CANNY_PRUNING) tArr.push_back(QString("CASCADE_DO_CANNY_PRUNING"));
    if(flags & cv::CASCADE_DO_ROUGH_SEARCH) tArr.push_back(QString("CASCADE_DO_ROUGH_SEARCH"));
    if(flags & cv::CASCADE_FIND_BIGGEST_OBJECT) tArr.push_back(QString("CASCADE_FIND_BIGGEST_OBJECT"));
    tObj.insert("flags",tArr);

    tObj2 = QJsonObject();
    tObj2.insert("width",minSize.width);
    tObj2.insert("height",minSize.height);
    tObj.insert("min_size",tObj2);

    tObj.insert("has_max_size",QString(hasMaxSize?"true":"false"));

    tObj2 = QJsonObject();
    tObj2.insert("width",maxSize.width);
    tObj2.insert("height",maxSize.height);
    tObj.insert("max_size",tObj2);

    return QJsonValue(tObj);
}

bool OptCascade::operator==(OptCascade &other){
    if(path != other.path) return false;
    if(scaleStep != other.scaleStep) return false;
    if(minNeighbors != other.minNeighbors) return false;
    if(flags != other.flags) return false;
    if(minSize != other.minSize) return false;
    if(hasMaxSize != other.hasMaxSize) return false;
    if(maxSize != other.maxSize) return false;
    return true;
}
