#ifndef OPTCASCADE_H
#define OPTCASCADE_H

#include <QJsonArray>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opt.h"

class OptCascade : public Opt
{
public:
    OptCascade();
    OptCascade(QJsonValue value);
    OptCascade(
            QString path,
            double scaleStep,
            int minNeighbors,
            int flags,
            cv::Size minSize,
            bool hasMaxSize,
            cv::Size maxSize
            );

    QString path;

    double scaleStep;
    int minNeighbors;
    int flags;
    cv::Size minSize;
    bool hasMaxSize;
    cv::Size maxSize;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();


    bool operator==(OptCascade &other);
    inline bool operator!=(OptCascade &other){return !(*this == other);};
};
#endif // OPTCASCADE_H
