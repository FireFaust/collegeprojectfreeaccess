#ifndef OPTRECT_H
#define OPTRECT_H

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opt.h"

class OptRect : public Opt
{

public:
    OptRect();
    OptRect(int x, int y, int width, int height);

    int x;
    int y;
    int width;
    int height;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    inline cv::Rect rect(){ return cv::Rect(x,y,width,height);}
    inline void setRect(cv::Rect rect){x = rect.x;y=rect.y;width=rect.width;height=rect.height;}

    bool operator==(OptRect &other);
    inline bool operator!=(OptRect &other){return !(*this == other);}
};

#endif // OPTRECT_H
