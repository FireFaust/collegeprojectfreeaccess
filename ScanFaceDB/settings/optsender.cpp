#include "optsender.h"

OptSender::OptSender()
{
    setDefaults();
}

void OptSender::setDefaults()
{
    frameSendUrl = "/OtwGateway/device/receive";
    rectGetUrl = "/OtwGateway/device/%1/settings/regions";
    minFrames = 5;
    maxFrames = 100;
    maxQueue = 200;
}

bool OptSender::setJSON(QJsonValue value)
{
    if(!value.isObject()){
        qWarning("Opt: Invalid type for sender");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== frame_send_url
    if(!getString(opt,frameSendUrl,"frame_send_url",true)) return false;
    //=============================================================== rect_get_url
    if(!getString(opt,rectGetUrl,"rect_get_url",true)) return false;
    //=============================================================== min_frames
    if(!getInt(opt,minFrames,"min_frames")) return false;
    //=============================================================== max_frames
    if(!getInt(opt,maxFrames,"max_frames")) return false;
    //=============================================================== max_queue
    if(!getInt(opt,maxQueue,"max_queue")) return false;
    //=============================================================== flags
    return true;
}

QJsonValue OptSender::getJSON()
{
    QJsonObject tObj;
    tObj.insert("frame_send_url",frameSendUrl);
    tObj.insert("rect_get_url",rectGetUrl);
    tObj.insert("max_frames",maxFrames);
    tObj.insert("min_frames",minFrames);
    tObj.insert("max_queue",maxQueue);
    return QJsonValue(tObj);
}

bool OptSender::operator==(OptSender &other){
    if(frameSendUrl != other.frameSendUrl) return false;
    if(rectGetUrl != other.rectGetUrl) return false;
    if(maxFrames != other.maxFrames) return false;
    if(minFrames != other.minFrames) return false;
    if(maxQueue != other.maxQueue) return false;
    return true;
}



