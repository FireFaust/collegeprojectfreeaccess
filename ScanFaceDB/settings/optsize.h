#ifndef OPTSIZE_H
#define OPTSIZE_H


#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opt.h"

class OptSize : public Opt
{
public:
    OptSize();
    OptSize(int width, int height);

    int width;
    int height;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    inline cv::Size size(){ return cv::Size(width,height);}
    inline void setSize(cv::Size size){width=size.width;height=size.height;}

    bool operator==(OptSize &other);
    inline bool operator!=(OptSize &other){return !(*this == other);}
};

#endif // OPTSIZE_H
