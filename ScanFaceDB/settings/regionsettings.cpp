#include "regionsettings.h"

RegionSettings::RegionSettings()
{
    setDefaults();
}

void RegionSettings::setDefaults()
{
    channelRect.setRect(cv::Rect(50,50,100,100));
    unifiedRect.setRect(cv::Rect(100,50,100,100));
}

bool RegionSettings::setJSON(QJsonValue value)
{
    if(!value.isObject()){
        qWarning("Opt: Invalid type for RegionSettings");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== tv_rect
    if(!getOpt(opt,channelRect,"channel_rect")) return false;
    //=============================================================== tv_rect_unif
    if(!getOpt(opt,unifiedRect,"unified_rect")) return false;
    return true;
}

QJsonValue RegionSettings::getJSON()
{
    QJsonObject tObj;
    tObj.insert("channel_rect",channelRect.getJSON());
    tObj.insert("unified_rect",unifiedRect.getJSON());
    return QJsonValue(tObj);
}

bool RegionSettings::operator==(RegionSettings &other){
    if(channelRect != other.channelRect) return false;
    if(unifiedRect != other.unifiedRect) return false;
    return true;
}
