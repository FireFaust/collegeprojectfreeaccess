INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


SOURCES +=  \
    $$PWD/smartsettings.cpp \
    $$PWD/opt.cpp \
    $$PWD/optprocessor.cpp \
    $$PWD/optcascade.cpp \
    $$PWD/optsender.cpp \
    $$PWD/optrect.cpp \
    $$PWD/optsize.cpp \


HEADERS += \
    $$PWD/smartsettings.h \
    $$PWD/opt.h \
    $$PWD/optprocessor.h \
    $$PWD/optcascade.h \
    $$PWD/optsender.h \
    $$PWD/optrect.h \
    $$PWD/optsize.h \
