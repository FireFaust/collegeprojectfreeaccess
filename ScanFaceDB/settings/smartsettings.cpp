#include "smartsettings.h"

SmartSettings::SmartSettings()
{
    setDefaults();
}

SmartSettings::~SmartSettings()
{
}

SmartSettings::SmartSettings(QString filename)
{
    if(!QFile::exists(filename)){
        setDefaults();
        dumpToFile(filename);
    }else{
        readFromFile(filename);
    }
}

void SmartSettings::setDefaults()
{
    enableWebui = true;
    serverUrl = "http://192.168.4.142:8080";
    grabTimer = 1000;
    processor = OptProcessor();
    sender = OptSender();
    rectUpdateIntervalS = 10;
}

bool SmartSettings::setJSON(QJsonValue value)
{
    QJsonObject tObj;
    bool set;

    if(!value.isObject()){
        qWarning("Opt: Invalid options - Must be JSON object");
        return false;
    }
    QJsonObject opt = value.toObject();

    //=============================================================== server_url
    if(!getString(opt,serverUrl,"server_url",true)) return false;
    //=============================================================== rect_update_interval_s
    if(!getInt(opt,rectUpdateIntervalS,"rect_update_interval_s",true)) return false;
    //=============================================================== grab_timer
    if(!getInt(opt,grabTimer,"grab_timer")) return false;
    //=============================================================== processor
    if(!getObject(opt,tObj,"processor",false,set)) return false;
    if(set){
        processor = OptProcessor();
        processor.setJSON(tObj);
    }
    //=============================================================== sender
    if(!getObject(opt,tObj,"sender",false,set)) return false;
    if(set){
        sender = OptSender();
        sender.setJSON(tObj);
    }
    //=============================================================== enable_webui
    if(!getBool(opt,enableWebui,"enable_webui")) return false;
    return true;
}

QJsonValue SmartSettings::getJSON()
{
    QJsonObject tObj;

    tObj.insert("server_url",serverUrl);
    tObj.insert("rect_update_interval_s",(double)rectUpdateIntervalS);
    tObj.insert("grab_timer",grabTimer);
    tObj.insert("processor",processor.getJSON());
    tObj.insert("sender",sender.getJSON());
    tObj.insert("enable_webui",QString(enableWebui?"true":"false"));
    return QJsonValue(tObj);
}

bool SmartSettings::operator==(SmartSettings &other){
    if(serverUrl != other.serverUrl) return false;
    if(rectUpdateIntervalS != other.rectUpdateIntervalS) return false;
    if(grabTimer != other.grabTimer) return false;
    if(processor != other.processor) return false;
    if(sender != other.sender) return false;
    if(enableWebui != other.enableWebui) return false;
    return true;
}
