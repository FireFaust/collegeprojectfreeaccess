#ifndef __CREATESAMPLES_UTILITY_HPP__
#define __CREATESAMPLES_UTILITY_HPP__
#define CV_VERBOSE 1

#define CV_RANDOM_INVERT 0x7FFFFFFF
/**
 * @brief cvCreateTrainingSamples
 * Create training samples applying random distortions to sample image and
 * store them in .vec file
 * @param filename .vec file name
 * @param imgfilename sample image file name
 * @param bgcolor background color for sample image
 * @param bgthreshold background color threshold. Pixels those colors are in range
 * [bgcolor-bgthreshold, bgcolor+bgthreshold] are considered as transparent
 * @param bgfilename background description file name.
 * If not NULL samples will be put on arbitrary background
 * @param count desired number of samples
 * @param invert if not 0 sample foreground pixels will be inverted
 * if invert == CV_RANDOM_INVERT then samples will be inverted randomly
 * @param maxintensitydev desired max intensity deviation of foreground samples pixels
 * @param maxxangle max X rotation angle
 * @param maxyangle max Y rotation angle
 * @param maxzangle max Z rotation angle
 * @param showsamples if not 0 samples will be shown
 * @param winwidth desired samples width
 * @param winheight desired samples height
 */
void cvCreateTrainingSamples(const char* filename,
                             const char* imgfilename, int bgcolor, int bgthreshold,
                             const char* bgfilename, int count,
                             int invert = 0, int maxintensitydev = 40,
                             double maxxangle = 1.1,
                             double maxyangle = 1.1,
                             double maxzangle = 0.5,
                             int showsamples = 0,
                             int winwidth = 24, int winheight = 24 );
void cvCreateTestSamples(  const char* infoname,
                           const char* imgfilename, int bgcolor, int bgthreshold,
                           const char* bgfilename, int count,
                           int invert, int maxintensitydev,
                           double maxxangle, double maxyangle, double maxzangle,
                           int showsamples,
                           int winwidth, int winheight );
/**
 * @brief cvCreateTrainingSamplesFromInfo
 * Create training samples from a set of marked up images and store them into .vec file
 * @param infoname file in which marked up image descriptions are stored
 * @param vecfilename .vec file name
 * @param num desired number of samples
 * @param showsamples if not 0 samples will be shown
 * @param winwidth sample width
 * @param winheight sample height
 * @return
 */
int cvCreateTrainingSamplesFromInfo( const char* infoname, const char* vecfilename,
                                     int num,
                                     int showsamples,
                                     int winwidth, int winheight );
/**
 * @brief cvShowVecSamples
 * Shows samples stored in .vec file
 * @param filename .vec file name
 * @param winwidth sample width
 * @param winheight sample height
 * @param scale the scale each sample is adjusted to
 */
void cvShowVecSamples( const char* filename, int winwidth, int winheight, double scale );
#endif //__CREATESAMPLES_UTILITY_HPP__
