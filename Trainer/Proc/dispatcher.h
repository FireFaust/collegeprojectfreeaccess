#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QMainWindow>

#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>

#include <QDebug>
#include <QFileDialog>
#include <QTextStream>
#include <QDateTime>

#include "directoryanalyzer.h"
#include "settingsfield.h"
#include "trainer.h"

using namespace cv;

namespace Ui {
class Dispatcher;
}

class Dispatcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Dispatcher(QWidget *parent = 0);
    ~Dispatcher();

private:
    Ui::Dispatcher *ui;

    QGridLayout *mainLayout;

    CascadeClassifier mFaceDetector;

    DirectoryAnalyzer *dirA;
    SettingsField *settingsField;
    Trainer *trainer;

    void drawFields();

    QStringList positiveList;
    QStringList negativeList;

    QString posDir;
    QString negDir;

    QPushButton *posButton;
    QPushButton *negButton;
    QPushButton *prepareButton;
    QPushButton *createSamples;
    QPushButton *trainingButton;

    QComboBox *sampleBox;
private slots:
    void searchPositive();
    void searchNegative();
    void prepare();
    void setSettings();
    void showSettings();
    void startSample();
};

#endif // DISPATCHER_H
