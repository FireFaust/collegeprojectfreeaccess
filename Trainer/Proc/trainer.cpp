#include "trainer.h"

Trainer::Trainer(QObject *parent) : QObject(parent)
{
    settings = new QSettings;
}

Trainer::~Trainer()
{

}

bool Trainer::doSample(QString type)
{
    qDebug() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str()<< "creating sample";
    QTime timer;
    timer.start();
    QFile::remove("my_positive.dat");
    QFile::copy(settings->value("info").toString(),"my_positive.dat");
    QFile::remove("my_negative.dat");
    QFile::copy(settings->value("bg").toString(),"my_negative.dat");
    char* nullname = (char*)"(NULL)";
    QString tStr;
    char* vecname = NULL; /* .vec file name */
    char* infoname = NULL; /* file name with marked up image descriptions */
    char* imagename = NULL; /* single sample image */
    char* bgfilename = NULL; /* background */
    int num;
    int bgcolor;
    int bgthreshold;
    int invert = 0;
    int maxintensitydev;
    double maxxangle;
    double maxyangle;
    double maxzangle;
    int showsamples =0;
    /* the samples are adjusted to this scale in the sample preview window */   
    int width;
    int height;
    double scale = 4.0;

    tStr = "my_positive.dat";
    infoname = new char[tStr.size()+1];
    strcpy(infoname,tStr.toStdString().c_str());

    vecName = "Vectors/vecfile_"+QDateTime::currentDateTime().toString("yyyy_dd_MM_hh:mm:ss:zzz")+".vec";
    vecname = new char[vecName.size()+1];
    strcpy(vecname,vecName.toStdString().c_str());

    tStr = settings->value("img").toString();
    imagename = new char[tStr.size()+1];
    strcpy(imagename,tStr.toStdString().c_str());

    tStr = "my_negative.dat";
    bgfilename = new char[tStr.size()+1];
    strcpy(bgfilename,tStr.toStdString().c_str());

    num = settings->value("num").toInt();
    bgcolor = settings->value("bgcolor").toInt();
    bgthreshold = settings->value("bgthresh").toInt();
    if(settings->value("inv")!="none")
        invert = 1;
    if(settings->value("randinv")!="none")
        invert = CV_RANDOM_INVERT;
    maxintensitydev = settings->value("maxidev").toDouble();
    maxxangle = settings->value("maxxangle").toDouble();
    maxyangle = settings->value("maxyangle").toDouble();
    maxzangle = settings->value("maxzangle").toDouble();

    if(settings->value("inv")!="none")
        invert = 1;
    if(settings->value("randinv")!="none")
        invert = CV_RANDOM_INVERT;

    if(settings->value("show")!="none")
        showsamples = 1;

    width = settings->value("w").toInt();

    height = settings->value("h").toInt();

    qDebug() <<"Info file name:" << ((infoname == NULL) ? nullname : infoname );
    qDebug() <<"Img file name:" << ((imagename == NULL) ? nullname : imagename );
    qDebug() <<"Vec file name:"<< ((vecname == NULL) ? nullname : vecname );
    qDebug() <<"BG file name:"<< ((bgfilename == NULL) ? nullname : bgfilename );
    qDebug() <<"Num:"<< num;
    qDebug() <<"BG color:"<< bgcolor;
    qDebug() <<"BG threshold:"<< bgthreshold;
    qDebug() <<"Invert:"<< ((invert == CV_RANDOM_INVERT) ? "RANDOM" : ( (invert) ? "TRUE" : "FALSE" )) ;
    qDebug() <<"Max intensity deviation:"<< maxintensitydev ;
    qDebug() <<"Max x angle:"<< maxxangle ;
    qDebug() <<"Max y angle:"<< maxyangle ;
    qDebug() <<"Max z angle:"<< maxzangle ;
    if(showsamples)
        qDebug() <<"Show samples: TRUE";
    else
        qDebug() <<"Show samples: FALSE";
    if( showsamples )
    {
        qDebug() << "Scale:"<< scale;
    }
    qDebug() << "Width:"<< width;
    qDebug() << "Height:"<< height;
    /* determine action */
    if(type == "Image set")
    {
        if(settings->value("info")=="none")
        {
            qDebug() << "no info specified" << "===> break";
            return false;
        }
        int total;
        qDebug() <<"Create training samples from images collection...";
        total = cvCreateTrainingSamplesFromInfo( infoname, vecname, num, showsamples,
        width, height );
        qDebug() <<"Done. Created"<<total<< "samples";
    }
    else
    {
        if(settings->value("img")=="none")
        {
            qDebug() << "no image specified" << "===> break";
            return false;
        }
        if( imagename && vecname )
        {
            qDebug() <<"Create training samples from single image applying distortions...";
            cvCreateTrainingSamples( vecname, imagename, bgcolor, bgthreshold, bgfilename,
            num, invert, maxintensitydev,
            maxxangle, maxyangle, maxzangle,
            showsamples, width, height );
            qDebug() << "Done";
        }
        else if( imagename && bgfilename && infoname )
        {
            qDebug() <<"Create test samples from single image applying distortions...";
            cvCreateTestSamples( infoname, imagename, bgcolor, bgthreshold, bgfilename, num,
            invert, maxintensitydev,
            maxxangle, maxyangle, maxzangle, showsamples, width, height );
            qDebug() <<"Done";
        }
    }
    qDebug()<<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str() << "Sample generation complited" << "time elapsed:" <<(double)timer.elapsed()/1000.0 <<"sec";
    return true;
}

void Trainer::doTraining()
{
    qDebug() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str()<< "starting training";

    QFile::remove("tempvec.vec");
    QFile::copy(settings->value("vec").toString(),"tempvec.vec");
    QFile::remove("my_negative.dat");
    QFile::copy(settings->value("bg").toString(),"my_negative.dat");

    QTime timer;
    timer.start();

    CvCascadeClassifier classifier;
    string cascadeDirName, vecName, bgName;
    int numPos    = 2000;
    int numNeg    = 1000;
    int numStages = 20;
    int precalcValBufSize = 256,
        precalcIdxBufSize = 256;
    bool baseFormatSave = false;

    CvCascadeParams cascadeParams;
    CvCascadeBoostParams stageParams;
    Ptr<CvFeatureParams> featureParams[] = { Ptr<CvFeatureParams>(new CvHaarFeatureParams),
                                             Ptr<CvFeatureParams>(new CvLBPFeatureParams),
                                             Ptr<CvFeatureParams>(new CvHOGFeatureParams)
                                           };

    cascadeDirName = settings->value("data").toString().toStdString();
    vecName = "tempvec.vec";
    bgName = "my_negative.dat";
    numPos = settings->value("numPos").toInt();
    numNeg = settings->value("numNeg").toInt();
    numStages = settings->value("numStages").toInt();
    precalcValBufSize = settings->value("precalcValBufSize").toInt();
    precalcIdxBufSize = settings->value("precalcIdxBufSize").toInt();
    if(settings->value("baseFormatSave")!="none")
        baseFormatSave = true;
    cascadeParams.scanAttr("-featureType",settings->value("featureType").toString().toStdString());
    cascadeParams.scanAttr("-w",settings->value("w").toString().toStdString());
    cascadeParams.scanAttr("-h",settings->value("h").toString().toStdString());
    stageParams.scanAttr("-bt",settings->value("bt").toString().toStdString());
    stageParams.scanAttr("-minHitRate",settings->value("minHitRate").toString().toStdString());
    stageParams.scanAttr("-maxFalseAlarmRate",settings->value("maxFalseAlarmRate").toString().toStdString());
    stageParams.scanAttr("-weightTrimRate",settings->value("weightTrimRate").toString().toStdString());
    stageParams.scanAttr("-maxDepth",settings->value("maxDepth").toString().toStdString());
    stageParams.scanAttr("-maxWeakCount",settings->value("maxWeakCount").toString().toStdString());
    featureParams[0]->scanAttr("-mode", settings->value("mode").toString().toStdString());

    setNumThreads(settings->value("numThreads").toInt());
    classifier.train( cascadeDirName,
                      vecName,
                      bgName,
                      numPos, numNeg,
                      precalcValBufSize, precalcIdxBufSize,
                      numStages,
                      cascadeParams,
                      *featureParams[cascadeParams.featureType],
                      stageParams,
                      baseFormatSave );

    qDebug()<<QDateTime::currentDateTime().toString("hh:mm:ss:zzz").toStdString().c_str() << "Training complited" << "time elapsed:" <<(double)timer.elapsed()/1000.0 <<"sec";
}
