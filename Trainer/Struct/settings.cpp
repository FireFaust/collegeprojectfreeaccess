#include "settings.h"

Settings::Settings(QObject *parent) : QObject(parent)
{
    load();
}

Settings::~Settings()
{

}

void Settings::load()
{
    file.setFileName("settings.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        setDefaults();
    jsonVal = file.readAll();
    file.close();
    setSettings("DEFAULT");
}

void Settings::setDefaults()
{
    file.close();
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"loading default settings";
    QFile pFile("settings.json");

    QJsonObject mainObj;

    QJsonObject sampleObj;
    sampleObj.insert("info",QStringLiteral("none"));
    sampleObj.insert("img",QStringLiteral("none"));
    sampleObj.insert("vec",QStringLiteral("none"));
    sampleObj.insert("bg",QStringLiteral("none"));
    sampleObj.insert("num",1000);
    sampleObj.insert("bgcolor",0);
    sampleObj.insert("bgthresh",80);
    sampleObj.insert("inv",QStringLiteral("none"));
    sampleObj.insert("randinv",QStringLiteral("none"));
    sampleObj.insert("maxidev",40);
    sampleObj.insert("maxxangle",1.1);
    sampleObj.insert("maxyangle",1.1);
    sampleObj.insert("maxzangle",0.5);
    sampleObj.insert("show",QStringLiteral("none"));
    sampleObj.insert("w",40);
    sampleObj.insert("h",32);

    QJsonObject trainObj;
    trainObj.insert("data",QStringLiteral("data/"));
    trainObj.insert("vec",QStringLiteral("none"));
    trainObj.insert("bg",QStringLiteral("none"));
    trainObj.insert("numPos",1000);
    trainObj.insert("numNeg",1000);
    trainObj.insert("numStages",15);
    trainObj.insert("precalcValBufSize",256);
    trainObj.insert("precalcIdxBufSize",256);
    trainObj.insert("baseFormatSave",QStringLiteral("none"));
    trainObj.insert("numThreads",QThread::idealThreadCount());
    trainObj.insert("featureType",QStringLiteral("HAAR"));
    trainObj.insert("w",40);
    trainObj.insert("h",32);
    trainObj.insert("bt",QStringLiteral("GAB"));
    trainObj.insert("minHitRate",0.995);
    trainObj.insert("maxFalseAlarmRate",0.5);
    trainObj.insert("weightTrimRate",0.95);
    trainObj.insert("maxDepth",1);
    trainObj.insert("maxWeakCount",100);
    trainObj.insert("mode",QStringLiteral("BASIC"));

    mainObj.insert("Sample",sampleObj);
    mainObj.insert("Training",trainObj);
    QJsonDocument doc = QJsonDocument(mainObj);
    pFile.open(QIODevice::WriteOnly);
    pFile.write(doc.toJson());
    pFile.close();
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"Complited";
    file.open(QIODevice::ReadOnly | QIODevice::Text);
}

void Settings::setSettings(QString path)
{
    if (path=="DEFAULT")
    {
        file.close();
        file.setFileName("settings.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        jsonVal = file.readAll();
        file.close();
        jDoc = QJsonDocument::fromJson(jsonVal.toUtf8());

        jObj = jDoc.object();
        jItem  = jValue.toObject();
    }
    else
    {
        file.close();
        file.setFileName(path);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        jsonVal = file.readAll();
        file.close();
        jDoc = QJsonDocument::fromJson(jsonVal.toUtf8());

        jObj = jDoc.object();
        jItem  = jValue.toObject();
    }

    jValue = jObj.value("Sample");
    jItem  = jValue.toObject();

    settings.setValue("Sample","Sample Settings");
    settings.setValue("info",jItem["info"].toString());
    settings.setValue("img",jItem["img"].toString());
    settings.setValue("vec",jItem["vec"].toString());
    settings.setValue("bg",jItem["bg"].toString());
    settings.setValue("num",jItem["num"].toDouble());
    settings.setValue("bgcolor",jItem["bgcolor"].toDouble());
    settings.setValue("bgthresh",jItem["bgthresh"].toDouble());
    settings.setValue("inv",jItem["inv"].toString());
    settings.setValue("randinv",jItem["randinv"].toString());
    settings.setValue("maxidev",jItem["maxidev"].toDouble());
    settings.setValue("maxxangle",jItem["maxxangle"].toDouble());
    settings.setValue("maxyangle",jItem["maxyangle"].toDouble());
    settings.setValue("maxzangle",jItem["maxzangle"].toDouble());
    settings.setValue("show",jItem["show"].toString());
    settings.setValue("w",jItem["w"].toDouble());
    settings.setValue("h",jItem["h"].toDouble());

    jValue = jObj.value("Training");
    jItem  = jValue.toObject();

    settings.setValue("Training","Training Settings");
    settings.setValue("data",jItem["data"].toString());
    settings.setValue("Tvec",jItem["vec"].toString());
    settings.setValue("Tbg",jItem["bg"].toString());
    settings.setValue("numPos",jItem["numPos"].toDouble());
    settings.setValue("numNeg",jItem["numNeg"].toDouble());
    settings.setValue("numStages",jItem["numStages"].toDouble());
    settings.setValue("precalcValBufSize",jItem["precalcValBufSize"].toDouble());
    settings.setValue("precalcIdxBufSize",jItem["precalcIdxBufSize"].toDouble());
    settings.setValue("baseFormatSave",jItem["baseFormatSave"].toString());
    settings.setValue("numThreads",jItem["numThreads"].toDouble());
    settings.setValue("featureType",jItem["featureType"].toString());
    settings.setValue("bt",jItem["bt"].toString());
    settings.setValue("minHitRate",jItem["minHitRate"].toDouble());
    settings.setValue("maxFalseAlarmRate",jItem["maxFalseAlarmRate"].toDouble());
    settings.setValue("weightTrimRate",jItem["weightTrimRate"].toDouble());
    settings.setValue("maxDepth",jItem["maxDepth"].toDouble());
    settings.setValue("maxWeakCount",jItem["maxWeakCount"].toDouble());
    settings.setValue("mode",jItem["mode"].toString());
}

void Settings::setNewSettings(QString path)
{
    if(path=="DEFAULT") path = "settings.json";
    file.close();
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"updating settings";
    QFile pFile(path);

    QJsonObject mainObj;

    QJsonObject sampleObj;
    sampleObj.insert("info",settings.value("info").toString());
    sampleObj.insert("img",settings.value("img").toString());
    sampleObj.insert("vec",settings.value("vec").toString());
    sampleObj.insert("bg",settings.value("bg").toString());
    sampleObj.insert("num",settings.value("num").toInt());
    sampleObj.insert("bgcolor",settings.value("bgcolor").toInt());
    sampleObj.insert("bgthresh",settings.value("bgthresh").toInt());
    sampleObj.insert("inv",settings.value("inv").toString());
    sampleObj.insert("randinv",settings.value("randinv").toString());
    sampleObj.insert("maxidev",settings.value("maxidev").toInt());
    sampleObj.insert("maxxangle",settings.value("maxxangle").toDouble());
    sampleObj.insert("maxyangle",settings.value("maxyangle").toDouble());
    sampleObj.insert("maxzangle",settings.value("maxzangle").toDouble());
    sampleObj.insert("show",settings.value("show").toString());
    sampleObj.insert("w",settings.value("w").toInt());
    sampleObj.insert("h",settings.value("h").toInt());

    QJsonObject trainObj;
    trainObj.insert("data",settings.value("data").toString());
    trainObj.insert("vec",settings.value("vec").toString());
    trainObj.insert("bg",settings.value("bg").toString());
    trainObj.insert("numPos",settings.value("numPos").toInt());
    trainObj.insert("numNeg",settings.value("numNeg").toInt());
    trainObj.insert("numStages",settings.value("numStages").toInt());
    trainObj.insert("precalcValBufSize",settings.value("precalcValBufSize").toInt());
    trainObj.insert("precalcIdxBufSize",settings.value("precalcIdxBufSize").toInt());
    trainObj.insert("baseFormatSave",settings.value("baseFormatSave").toString());
    trainObj.insert("numThreads",settings.value("numThreads").toInt());
    trainObj.insert("featureType",settings.value("featureType").toString());
    trainObj.insert("w",settings.value("w").toInt());
    trainObj.insert("h",settings.value("h").toInt());
    trainObj.insert("bt",settings.value("bt").toString());
    trainObj.insert("minHitRate",settings.value("minHitRate").toDouble());
    trainObj.insert("maxFalseAlarmRate",settings.value("maxFalseAlarmRate").toDouble());
    trainObj.insert("weightTrimRate",settings.value("weightTrimRate").toDouble());
    trainObj.insert("maxDepth",settings.value("maxDepth").toDouble());
    trainObj.insert("maxWeakCount",settings.value("maxWeakCount").toInt());
    trainObj.insert("mode",settings.value("mode").toString());

    mainObj.insert("Sample",sampleObj);
    mainObj.insert("Training",trainObj);
    QJsonDocument doc = QJsonDocument(mainObj);
    pFile.open(QIODevice::WriteOnly);
    pFile.write(doc.toJson());
    pFile.close();
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"Complited";
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    setSettings(path);
}

long Settings::getMem()
{
    QProcess p;
    p.start("awk", QStringList() << "/MemTotal/ { print $2 }" << "/proc/meminfo");
    p.waitForFinished();
    QString memory = p.readAllStandardOutput();
    p.close();
    return (memory.toLong() / 1024);
}
