#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

#include <QSettings>
#include <QFile>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonValueRef>

#include <QDateTime>
#include <QThread>
#include <QProcess>
#include <sys/sysinfo.h>

#include <QDebug>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);
    ~Settings();

    QSettings settings;

    QFile file;
    QString jsonVal;
    QJsonDocument jDoc;
    QJsonObject   jObj;
    QJsonValue    jValue;
    QJsonObject   jItem;

    void setDefaults();
    void setSettings(QString path);
    void setNewSettings(QString path);
    long getMem();
private:
    void load();
signals:

public slots:
};

#endif // SETTINGS_H
