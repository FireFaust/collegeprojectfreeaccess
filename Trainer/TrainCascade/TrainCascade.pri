INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/boost.h \
    $$PWD/cascadeclassifier.h \
    $$PWD/haarfeatures.h \
    $$PWD/HOGfeatures.h \
    $$PWD/imagestorage.h \
    $$PWD/lbpfeatures.h \
    $$PWD/traincascade_features.h

SOURCES += \
    $$PWD/boost.cpp \
    $$PWD/cascadeclassifier.cpp \
    $$PWD/features.cpp \
    $$PWD/haarfeatures.cpp \
    $$PWD/HOGfeatures.cpp \
    $$PWD/imagestorage.cpp \
    $$PWD/lbpfeatures.cpp
